

# 說明文件

## myblog
first django project

## 執行網站
1. 執行虛擬環境
   - `/venv/Scripts/activate`
   - cmd：`/venv/Scripts/activate.bat`
   - Power Shell：`/venv/Scripts/activate.ps1`
2. 進入專案資料夾
   - `cd myblog`
3. 執行專案
   - `python manage.py runserver 127.0.0.1:8000`

   <img src='myblog/static/images/p2.png'>

詳情可參考老師之code
https://gitlab.com/shiunyi71/cht_django2023